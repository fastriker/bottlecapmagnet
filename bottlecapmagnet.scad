use <threads.scad>;

thick = 1;
gap = 0.5;
diameter_thread = 28;
diameter_magnet = 8;
magnet_height = 3;
// Mate: 12
// Water: 14
height = 12;
spacer_height = 4;
spacer_diameter = 24;
tool_diameter = 2;
number_tool_holes = 2;

difference()
{
    intersection()
    {
        metric_thread (diameter_thread,  3, height, internal=false);
        union()
        {
            cylinder(h=height-spacer_height, d=diameter_thread+1);
            translate([0, 0, height - spacer_height])
            {
                cylinder(h=spacer_height, d1=diameter_thread, d2=spacer_diameter);
            }
        }
    }  
    translate([0, 0, thick])
    {
        cylinder(h=height, d=diameter_magnet + gap);
    }
    translate([0, 0, -thick])
    {
        for(deg = [0 : 360/number_tool_holes : 360])
        {
            rotate([0, 0, deg])
            {
                translate([diameter_magnet/2+(diameter_thread-diameter_magnet)/4, 0, 0])
                {
                    cylinder(h=height-tool_diameter, d=tool_diameter);
                    translate([0, 0, height-tool_diameter])
                    {
//                        cylinder(h=tool_diameter, d1=tool_diameter, d2=0);
                    }
                }
            }
        }
    }
}

translate([(diameter_thread + diameter_magnet)/2 + 5, 0, 0])
{
    difference()
    {
        cylinder(h=height - thick - magnet_height - gap, d=diameter_magnet);
        translate([-diameter_magnet/2, -1, height - thick - magnet_height - gap - 2])
        {
            cube([diameter_magnet, 2, 2]);
        }
    }
}