# Bottle Cap Magnet

This is a OpenSCAD model for a bottle cap magnet. It describes a body, that fix a magnet in a threaded bottle cap.
STL-files can be downloaded [via Thingiverse](https://www.thingiverse.com/thing:3301334).